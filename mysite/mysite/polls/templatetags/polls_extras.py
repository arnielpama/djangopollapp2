import math
from django import template

register = template.Library()

@register.filter(name='computePercent')
def computePercent(value, arg):
    val = (arg/value) * 100
    return math.ceil(val)
    #arg being choice.votes, and value being totalvotes