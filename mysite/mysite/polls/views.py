from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Question, Choice

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.filter(
        	 pub_date__lte=timezone.now()
       	).order_by('-pub_date')


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/index.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

    def get_context_data(self, **kwargs):
        context = super(ResultsView, self).get_context_data(**kwargs)
        votes = 0
        choices = Choice.objects.filter(question__id=self.kwargs['pk'])
        for choice in choices:
        	votes = votes + choice.votes
        context['totalvotes'] = votes
        
        return context

def vote(request, question_id):
	question = get_object_or_404(Question, pk=question_id)
	try:
		selected_choice = question.choice_set.get(pk=request.POST['choice'])
	except (KeyError, Choice.DoesNotExist):
		pass  #alert is handled in javascript
	else:
		selected_choice.votes += 1;
		selected_choice.save()
		return HttpResponseRedirect(reverse('polls:results', args=(question_id,)))