from django.urls import path
from django.contrib.auth.views import password_reset
from django.conf.urls import url

from . import views

app_name = 'accounts'
urlpatterns = [
    path('signup/', views.signup, name='signup'),
    url(r'password_reset/$', password_reset, {'template_name': 'registration/password_reset.html'}, name="password_reset"),
]